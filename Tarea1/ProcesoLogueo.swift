//
//  ProcesoLogueo.swift
//  Tarea1
//
//  Created by macbook on 1/23/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import Foundation

class ProcesoLogueo{
    
    func Login(email:String, clave:String)->String{
        
        let emailExito:String = "ejemplo@dominio.com" 
        let claveExito:String = "12345"
        var mensajeRetorno = " "
        
        let validarEmail = validateEmail(enteredEmail: email)    //Valido formato de email
        
        if(validarEmail){
            if( (email  == String(emailExito) )  && (clave  == String(claveExito)) ){
                mensajeRetorno = "Gracias por ingresar al sistema con el correo ejemplo@dominio.com"
            }else{
                mensajeRetorno = "Su usuario o clave no coinciden"
            }
        }else{
            mensajeRetorno = "El email no es válido. Vuelva a ingresar su email"
        }
        
        return mensajeRetorno
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
}
