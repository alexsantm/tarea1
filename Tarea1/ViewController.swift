//
//  ViewController.swift
//  Tarea1
//
//  Created by macbook on 1/23/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtClave: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtResultado: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func Login(_ sender: Any) {
        
        guard let email = txtEmail.text else { return  }
        guard let clave = txtClave.text else { return  }
        
        // LLamo al método Login en la clase ProcesoLogueo para ejecutar la logica
        let mensajeResultado = ProcesoLogueo().Login(email: email, clave: clave)
        
        txtResultado.text = mensajeResultado
        
        if(mensajeResultado == "Gracias por ingresar al sistema con el correo ejemplo@dominio.com"){
            txtResultado.textColor = UIColor.green      //Color verde para mensaje de exito
            imgAvatar.image = UIImage(named: "user")
        }else{
            txtResultado.textColor = UIColor.red         //Color rojo para mensaje de fallo
            imgAvatar.image = UIImage(named: "avatar")
        }
        
    }
    
}

